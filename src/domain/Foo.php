<?php


namespace domain;


use Psr\Log\LoggerInterface;
use Seld\JsonLint\JsonParser;

class Foo
{
    private $logger;

    public function __construct(LoggerInterface $logger = null)
    {
        $this->logger = $logger;
    }

    public function doSomething()
    {
        echo('ParseJson');
        $parser = new JsonParser();
        $result = $parser->parse('{"name":"John", "age":30, "car":null}');
        var_dump($result);

        if ($this->logger) {
            $this->logger->info('Doing work');
        }
    }
}